variable "deployment_id" {
  description = "Module deployment ID (may be generated with terraform-deployment-id module)"
}

variable "hostname" {
  description = "Droplet name (can only contain alphanumeric characters, dashes, and periods)"
}

variable "size" {
  description = "Size slug. Get available sizes with: doctl compute size list"
  default     = "s-1vcpu-1gb"
}

variable "region" {
  description = "Datacenter region slug where the Droplet will be created. Get available regions with: doctl compute region list"
  default     = "ams3"
}

variable "backups" {
  description = "Enable automatic system-level backups for a 20% price of the Droplet"
  default     = "false"
}

variable "sshd_port" {
  description = "Just say NO to port 22!"
  default     = "22"
}

variable "timezone" {
  description = "Droplet Timezone to set. Get available timezones with: timedatectl list-timezones"
  default     = "Etc/UTC"
}

variable "reboot" {
  description = "Reboot Droplet when done (set to false if you need to run your own scripts with remote-exec)"
  default     = "true"
}
