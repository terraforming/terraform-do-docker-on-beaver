output "id" {
  description = "Droplet ID"
  value       = "${digitalocean_droplet.beaver.id}"
}

output "hostname" {
  description = "Droplet hostname"
  value       = "${digitalocean_droplet.beaver.name}"
}

output "public_ip" {
  description = "Droplet public IP address (ipv4)"
  value       = "${digitalocean_droplet.beaver.ipv4_address}"
}

output "ssh_key" {
  description = "Generated SSH key"
  value       = "${local.ssh_key}"
}

output "password" {
  description = "Generated file with a password"
  value       = "${local.password}"
}
