# DigitalOcean Droplet Terraform module

Ubuntu 18.04 (Bionic Beaver) running latest Docker.

## Features

* Can be run from Linux or Windows (GitBash)
* SSH key generated for each new Droplet (not stored in Terraform state)
* Packages upgraded to the latest version
* Running latest Docker CE
* Default user **ubuntu** with a generated password (not stored in Terraform state)
* Sudo with a password
* SSHD running on the non-standard port (optionally)
* Timezone set (optionally)

## Requirements

* DigitalOcean [account](https://m.do.co/c/cb8c6d8114de) (referral link)
* DigitalOcean [API token](https://www.digitalocean.com/docs/api/create-personal-access-token/)
* `ssh-keygen`
* [GitBash](https://gitforwindows.org/) (on Windows)

## Usage

    module "new_droplet" {
        source        = "git::https://bitbucket.org/terraforming/terraform-do-docker-on-beaver.git?ref=master"
        deployment_id = "SOMETHING_UNIQUE"
        hostname      = "new-droplet"
    }

## Outputs

| Name | Description |
|------|-------------|
| id | Droplet ID |
| hostname | Droplet hostname |
| public_ip | Droplet public IP address (ipv4) |
| ssh_key | Generated SSH key |
| password | Generated file with a password |

## Variables

| Name | Default | Description |
|------|---------|-------------|
| deployment_id | | Module deployment ID (may be generated with [terraform-deployment-id](https://bitbucket.org/terraforming/terraform-deployment-id) module) |
| hostname | | Droplet name (can only contain alphanumeric characters, dashes, and periods) |
| size | s-1vcpu-1gb | Size slug. Get available sizes with: doctl compute size list |
| region | ams3 | Datacenter region slug where the Droplet will be created. Get available regions with: doctl compute region list |
| backups | false | Enable automatic system-level backups for a 20% price of the Droplet |
| sshd_port | 22 | Just say NO to port 22! |
| timezone | Etc/UTC | Droplet Timezone to set. Get available timezones with: timedatectl list-timezones |
| reboot | true | Reboot Droplet when done (set to false if you need to run your own scripts with remote-exec) |

## Tips

### DigitalOcean evangelism

[DigitalOcean](https://m.do.co/c/cb8c6d8114de) (referral link) is AWS with a human face. Within AWS you need to setup permissions (IAM), networking (VPC), understand subnets etc.,
in DO world you are two clicks (exactly!) from the running instance (Droplet).

#### Pros

* Easy services, nice and clear web management console
* Clear pricing
* Easy and fast API, Spaces API compatible with S3 (your tools will works!)

#### Cons

* Limited number of services, just a computation (Droplets) and storage (Volumes and Spaces)
* Paying also for **stopped** Droplets (you need to create Snapshot and delete a Droplet to actually save a money)
* API is not supporting all resources (e.g. Spaces creation) but it is changing rapidly

#### Pricing

For the running instance (Droplet) the price is almost the same as an AWS Reserved price (you can get kind of t2.medium for $20/month).

For Spaces (API compatible with S3) the price is $5/month for 250 GB including 1 TB outbound transfer.

Sometimes it is cheaper, sometimes much more expensive than AWS. E.g. it doesn't make a sense to run Linux Desktop you need to be running only a couple of hours per day in DO, AWS is much cheaper (you don't pay for stopped instances).

### Credentials

Following credentials were generated before Droplet startup. It is a good idea to backup them :)

* SSH Key

    Generated SSH key is stored in `.ssh` subdirectory of the Terraform root module.  
    Filename is `.ssh/id_rsa-do-${deployment_id}`.

* Password

    Generated password for **ubuntu** user is stored in `.ssh` subdirectory of the Terraform root module.  
    Filename is `.ssh/password-do-${deployment_id}`.

Note: Droplet is tagged with Module deployment ID.

### Docker

User **ubuntu** is in the _docker_ group and can run `docker` command without sudo.

### Sudo

User **ubuntu** can use a `sudo` command with a generated password. Password is in `.ssh/password-do-${deployment_id}`.

### Additional setup

If you would need to run your own scripts in the created Droplet, you may use `null_resource`.

1. Set the `reboot` to **false**

        module "new_droplet" {
            source        = "git::https://bitbucket.org/terraforming/terraform-do-docker-on-beaver.git?ref=master"
            deployment_id = "SOMETHING_UNIQUE"
            hostname      = "new-droplet"
            reboot        = "false"
        }

2. Run your script with `remote-exec` and (optionally) reboot Droplet yourself

        resource "null_resource" "setup-droplet" {
            provisioner "remote-exec" {
                inline = [
                    # Run your own script
                    "touch /tmp/have-been-here",
                    "shutdown --reboot +1",
                ]
                connection {
                    host        = "${module.new_droplet.public_ip}"
                    # Port is still 22 until reboot or SSHD is restarted
                    port        = "22"
                    # Root login is still working until reboot or SSHD is restarted
                    user        = "root"
                    private_key = "${file("${path.module}/${module.new_droplet.ssh_key}")}"
                }
            }
            depends_on = ["module.new_droplet"]
        }

### SSH command

Add SSH command to outputs (to use from scripts)

    output "ssh_cmd" {
        value = "ssh -i ${module.new_droplet.ssh_key} -p ${var.sshd_port} ubuntu@${module.new_droplet.public_ip}"
    }
