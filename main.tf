locals {
  # Get available images with: doctl compute image list-distribution
  image = "ubuntu-18-04-x64"

  ssh_key  = ".ssh/id_rsa-do-${var.deployment_id}"
  password = ".ssh/password-do-${var.deployment_id}"

  # For better compatibility the path elements on Win must use only simple slashes
  path_root   = "${replace(path.root, "\\", "/")}"
  path_module = "${replace(path.module, "\\", "/")}"
}

# Not using tls_private_key resource to keep the key only on local machine
# https://www.terraform.io/docs/providers/tls/r/private_key.html
resource "null_resource" "ssh_keygen" {
  triggers = {
    deployment_id = "${var.deployment_id}"
  }

  provisioner "local-exec" {
    command     = "mkdir --parents $(${local.path_module}/cygpath ${local.path_root}/.ssh)"
    interpreter = ["bash", "-c"]
  }

  provisioner "local-exec" {
    command     = "echo n | ssh-keygen -t rsa -b 4096 -C '${var.deployment_id}' -f $(${local.path_module}/cygpath ${local.path_root}/${local.ssh_key}) -N '' || true"
    interpreter = ["bash", "-c"]
  }
}

resource "digitalocean_ssh_key" "default" {
  name       = "${var.hostname} Droplet (${var.deployment_id})"
  public_key = "${file("${local.path_root}/${local.ssh_key}.pub")}"

  depends_on = ["null_resource.ssh_keygen"]
}

resource "digitalocean_tag" "default" {
  name = "deployment_id-${var.deployment_id}"
}

resource "digitalocean_tag" "allow_ssh" {
  name = "fw-allow-ssh"
}

resource "random_string" "password" {
  keepers = {
    deployment_id = "${var.deployment_id}"
  }

  length = 16
}

resource "null_resource" "password_gen" {
  triggers = {
    deployment_id = "${var.deployment_id}"
  }

  provisioner "local-exec" {
    command     = "mkdir --parents $(${local.path_module}/cygpath ${local.path_root}/.ssh)"
    interpreter = ["bash", "-c"]
  }

  provisioner "local-exec" {
    command     = "echo '${random_string.password.result}' > $(${local.path_module}/cygpath ${local.path_root}/${local.password})"
    interpreter = ["bash", "-c"]
  }
}

module "provisioner_files" {
  source = "git::https://bitbucket.org/terraforming/terraform-docker-on-beaver.git?ref=master"
}

resource "digitalocean_droplet" "beaver" {
  image = "${local.image}"

  name   = "${var.hostname}"
  size   = "${var.size}"
  region = "${var.region}"

  backups = "${var.backups}"

  ssh_keys = ["${digitalocean_ssh_key.default.id}"]

  tags = [
    "${digitalocean_tag.default.id}",
    "${digitalocean_tag.allow_ssh.id}",
  ]

  lifecycle {
    prevent_destroy = false
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir --parents /var/tmp/terraform-do-docker-on-beaver",
    ]

    connection {
      private_key = "${file("${path.root}/${local.ssh_key}")}"
    }
  }
}

resource "null_resource" "provisioner_upload" {
  count = "${length(module.provisioner_files.scripts)}"

  triggers {
    deployment_id = "${var.deployment_id}"
    index         = "${count.index}"
  }

  provisioner "file" {
    content     = "${module.provisioner_files.scripts[count.index]}"
    destination = "/var/tmp/terraform-do-docker-on-beaver/${count.index}-provisioner-script.sh"

    connection {
      host        = "${digitalocean_droplet.beaver.ipv4_address}"
      private_key = "${file("${path.root}/${local.ssh_key}")}"
    }
  }

  depends_on = ["digitalocean_droplet.beaver", "module.provisioner_files"]
}

resource "null_resource" "provisioner_launch" {
  triggers = {
    deployment_id = "${var.deployment_id}"
  }

  provisioner "file" {
    content     = "${module.provisioner_files.launcher}"
    destination = "/var/tmp/terraform-do-docker-on-beaver/launcher.sh"

    connection {
      host        = "${digitalocean_droplet.beaver.ipv4_address}"
      private_key = "${file("${path.root}/${local.ssh_key}")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd /var/tmp/terraform-do-docker-on-beaver",
      "chmod +x *.sh",
      "./launcher.sh '${var.timezone}' '${random_string.password.result}' '${var.sshd_port}' '${var.reboot}'",
    ]

    connection {
      host        = "${digitalocean_droplet.beaver.ipv4_address}"
      private_key = "${file("${path.root}/${local.ssh_key}")}"
    }
  }

  depends_on = ["null_resource.provisioner_upload"]
}
